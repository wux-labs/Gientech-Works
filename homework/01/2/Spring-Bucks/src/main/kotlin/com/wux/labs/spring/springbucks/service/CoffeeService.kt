package com.wux.labs.spring.springbucks.service

import com.baomidou.mybatisplus.extension.service.IService
import com.wux.labs.spring.springbucks.entity.Coffee

interface CoffeeService : IService<Coffee> {
    /**
     * 分页查询
     */
    fun listByPage(pageNum: Long, pageSize: Long): MutableList<Coffee>

    /**
     * 创建记录
     */
    fun create(coffee: Coffee): Boolean

    /**
     * 修改记录
     */
    fun update(coffee: Coffee): Int

    /**
     * 删除记录
     */
    fun delete(id: Long): Int

    /**
     * 无侵入式校验拦截
     */
    fun interceptFilter(sql: String)
}