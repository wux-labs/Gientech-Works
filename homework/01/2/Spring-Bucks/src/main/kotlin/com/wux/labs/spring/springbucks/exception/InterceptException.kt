package com.wux.labs.spring.springbucks.exception

/**
 * 自定义拦截异常类
 */
class InterceptException(message: String) : Exception(message) {
}