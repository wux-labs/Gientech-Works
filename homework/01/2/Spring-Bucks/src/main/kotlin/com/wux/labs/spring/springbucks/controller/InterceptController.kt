package com.wux.labs.spring.springbucks.controller

import com.wux.labs.spring.springbucks.service.CoffeeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.UncategorizedSQLException
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api("拦截接口")
@RestController
@RequestMapping("/intercept")
class InterceptController {
    @Autowired
    private lateinit var service: CoffeeService

    @ApiOperation("无侵入式校验拦截", notes = "需要手工输入完整是SQL语句进行拦截校验。")
    @GetMapping("/interceptFilter")
    fun interceptFilter(sql: String): String {
        return try {
            service.interceptFilter(sql)
            "执行成功"
        } catch (e: UncategorizedSQLException) {
            e.rootCause?.message ?: "SQL语法错误"
        } catch (e: Exception) {
            e.message ?: "SQL语法错误"
        }
    }
}