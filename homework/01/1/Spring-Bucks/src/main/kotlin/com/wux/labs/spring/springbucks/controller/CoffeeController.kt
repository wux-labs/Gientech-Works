package com.wux.labs.spring.springbucks.controller

import com.wux.labs.spring.springbucks.entity.Coffee
import com.wux.labs.spring.springbucks.service.CoffeeService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.joda.money.CurrencyUnit
import org.joda.money.Money
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.web.bind.annotation.*
import java.util.*

@Api("咖啡接口")
@RestController
@RequestMapping("/coffee")
@CacheConfig(cacheNames = ["coffee"])
class CoffeeController {
    @Autowired
    private lateinit var service: CoffeeService

    @ApiOperation("查询所有")
    @GetMapping("/list")
    @Cacheable(key = "'all'")
    fun list(): MutableList<Coffee> {
        return service.list()
    }

    @ApiOperation("分页查询")
    @PostMapping("/listByPage")
    @Cacheable(key = "'page'")
    fun listByPage(pageNum: Long, pageSize: Long): MutableList<Coffee> {
        return service.listByPage(pageNum, pageSize)
    }

    @ApiOperation("批量查询")
    @PostMapping("/listByIds")
    @Cacheable(key = "'ids'")
    fun listByIds(vararg ids: Long): MutableList<Coffee> {
        return service.listByIds(ids.toList())
    }

    @ApiOperation("完整插入")
    @PutMapping("/create")
    fun create(coffee: Coffee): Boolean {
        return service.create(coffee)
    }

    @ApiOperation("简单插入", notes = "当输入 name = 'rollback' 时，事务会回滚。")
    @PutMapping("/simpleCreate")
    fun simpleCreate(name: String, price: Double): String {
        return try {
            service.create(Coffee().also {
                it.name = name
                it.price = Money.of(CurrencyUnit.of("CNY"), price)
                it.createTime = Date()
                it.updateTime = Date()
            })
            "成功插入记录！"
        } catch (e: Exception) {
            return e.message!!
        }
    }

    @ApiOperation("简单更新", notes = "当输入 name = 'rollback' 时，事务会回滚。")
    @PostMapping("/simpleUpdate")
    fun simpleUpdate(id: Long, name: String, price: Double): String {
        return try {
            var update = service.update(Coffee().also {
                it.id = id
                it.name = name
                it.price = Money.of(CurrencyUnit.of("CNY"), price)
                it.updateTime = Date()
            })
            "成功更新了${update}条记录！"
        } catch (e: Exception) {
            e.message!!
        }
    }

    @ApiOperation("查询记录")
    @GetMapping("/selectById")
    @Cacheable(key = "#id")
    fun selectById(id: Long): Coffee {
        return service.getById(id)
    }

    @ApiOperation("删除记录", notes = "当删除的 id 在 [1,2,3,4,5] 中时，事务会回滚。")
    @DeleteMapping("/deleteById")
    @CacheEvict(key = "#id")
    fun deleteById(id: Long): String {
        return try {
            var delete = service.delete(id)
            "成功删除了${delete}条记录！"
        } catch (e: Exception) {
            e.message!!
        }
    }

    @ApiOperation("清空缓存")
    @DeleteMapping("/clearCache")
    @CacheEvict(allEntries = true)
    fun clearCache() {
    }
}