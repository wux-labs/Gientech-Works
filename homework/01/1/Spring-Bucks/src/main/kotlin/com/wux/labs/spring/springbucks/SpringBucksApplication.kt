package com.wux.labs.spring.springbucks

import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
@MapperScan("com.wux.labs.spring.springbucks.mapper")
class SpringBucksApplication

fun main(args: Array<String>) {
    runApplication<SpringBucksApplication>(*args)
}
