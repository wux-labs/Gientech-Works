package com.wux.labs.spring.springbucks.exception

/**
 * 自定义回滚异常类
 */
class RollbackException(message: String) : Exception(message) {
}