package com.wux.labs.spring.springbucks.mapper

import com.baomidou.mybatisplus.core.mapper.BaseMapper
import com.wux.labs.spring.springbucks.entity.Coffee
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select

@Mapper
interface CoffeeMapper : BaseMapper<Coffee> {
    /**
     * 分页查询
     */
    @Select("select * from t_coffee order by id")
    fun listByPage(@Param("pageNum") pageNum: Long, @Param("pageSize") pageSize: Long): MutableList<Coffee>
}