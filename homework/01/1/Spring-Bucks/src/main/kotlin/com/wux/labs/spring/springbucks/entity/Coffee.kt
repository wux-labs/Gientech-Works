package com.wux.labs.spring.springbucks.entity

import com.baomidou.mybatisplus.annotation.TableName
import com.baomidou.mybatisplus.extension.activerecord.Model
import com.fasterxml.jackson.annotation.JsonFormat
import io.swagger.annotations.ApiModel
import org.joda.money.Money
import org.springframework.format.annotation.DateTimeFormat
import java.util.*

@TableName("T_COFFEE")
@ApiModel(value = "咖啡表")
class Coffee : Model<Coffee>() {

    var id: Long? = null

    var name: String? = null

    var price: Money? = null

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    var createTime: Date? = null

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    var updateTime: Date? = null
}