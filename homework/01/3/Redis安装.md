# Redis安装

## Redis软件安装

为了环境隔离，使用Docker容器进行Redis软件的安装。

```
docker run -it --rm centos:centos7.6.1810
yum install -y wget gcc g++ make
```

* 下载软件

```
wget https://download.redis.io/releases/redis-6.2.6.tar.gz
```

* 解压软件

```
tar -xzf redis-6.2.6.tar.gz
```

* 编译安装

```
cd redis-6.2.6
make

mkdir bin
cp src/redis-cli bin/
cp src/redis-server bin/
cp src/redis-sentinel bin/
```

* 配置

`redis.conf`是Redis的配置文件

```
bind 127.0.0.1
port 6379
daemonize no
```

* 启动

```
bin/redis-server redis.conf
```

* 验证

```
bin/redis-cli -p 6379
```

## 单节点模式

主机地址：172.31.0.101

* 配置

`redis.conf`

```
port 6379
```

* 启动

```
redis-server redis.conf
```

* 验证

```
redis-cli -p 6379 info

docker exec -it redis-standalone redis-cli -p 6379 info
docker exec -it redis-standalone redis-cli -p 6379 info Replication
```



## 主从模式

主节点：172.32.0.101

从节点：172.32.0.102、172.32.0.103

* 配置

`redis-master.conf`

```
port 6379
protected-mode no

repl-diskless-sync no
repl-disable-tcp-nodelay no
```

`redis-slave.conf`

```
port 6379
protected-mode no

replicaof 172.32.0.101 6379

slave-read-only yes
slave-serve-stale-data yes
```

* 启动

```
# 主节点
redis-server redis-master.conf
# 从节点
redis-server redis-slave.conf
```

* 验证

```
redis-cli -p 6379 info

docker exec -it redis-replica-master redis-cli -p 6379 info Replication

docker exec -it redis-replica-slave1 redis-cli -p 6379 info Replication
docker exec -it redis-replica-slave2 redis-cli -p 6379 info Replication
```



## 哨兵模式

主节点：172.33.0.101

从节点：172.33.0.102、172.33.0.103

哨兵节点：172.33.0.104、172.33.0.105、172.33.0.106

* 配置

`redis-master.conf`

```
port 6379
protected-mode no

repl-diskless-sync no
repl-disable-tcp-nodelay no
```

`redis-slave.conf`

```
port 6379
protected-mode no

replicaof 172.33.0.101 6379

slave-read-only yes
slave-serve-stale-data yes
```

`redis-sentinel.conf`

```
port 26379

sentinel monitor mymaster 172.33.0.101 6379 2
sentinel failover-timeout mymaster 180000
```

* 启动

```
# 主节点
redis-server redis-master.conf
# 从节点
redis-server redis-slave.conf
# 哨兵节点
redis-sentinel redis-sentinel.conf
```

* 验证

```
redis-cli -p 6379 info
redis-cli -p 26379 info

docker exec -it redis-sentinel-master redis-cli -p 6379 info Replication

docker exec -it redis-sentinel-slave1 redis-cli -p 6379 info Replication
docker exec -it redis-sentinel-slave2 redis-cli -p 6379 info Replication

docker exec -it redis-sentinel-sentinel1 redis-cli -p 26379 info Sentinel
docker exec -it redis-sentinel-sentinel2 redis-cli -p 26379 info Sentinel
docker exec -it redis-sentinel-sentinel3 redis-cli -p 26379 info Sentinel

docker stop redis-sentinel-master
```



## 集群模式

主机地址：172.34.0.101、172.34.0.102、172.34.0.103、172.34.0.104、172.34.0.105、172.34.0.106

* 配置

`redis-cluster.conf`

```
port 6379
protected-mode no

cluster-enabled yes
cluster-config-file nodes.conf
cluster-node-timeout 15000
cluster-announce-bus-port 16379
```

* 启动

```
# 所有节点启动Redis
redis-server redis-cluster.conf
# 将所有Redis组成集群
redis-cli --cluster create 172.34.0.101:6379 172.34.0.102:6379 172.34.0.103:6379 172.34.0.104:6379 172.34.0.105:6379 172.34.0.106:6379 --cluster-replicas 1
```

* 验证

```
redis-cli -p 6379 info

docker exec -it redis-cluster-node1 redis-cli -p 6379 info
docker exec -it redis-cluster-node2 redis-cli -p 6379 info
docker exec -it redis-cluster-node3 redis-cli -p 6379 info
```

