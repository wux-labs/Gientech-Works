import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.7.5"
	id("io.spring.dependency-management") version "1.0.15.RELEASE"
	kotlin("jvm") version "1.6.21"
	kotlin("plugin.spring") version "1.6.21"
}

allprojects {
	apply(plugin = "org.springframework.boot")
	apply(plugin = "io.spring.dependency-management")
	apply(plugin = "java")
	apply(plugin = "kotlin")

	repositories {
		mavenCentral()
	}

	group = "com.wux.labs.spring"
	version = "0.0.1"

	dependencies {
		implementation("org.springframework.boot:spring-boot-starter-web")
		implementation("org.springframework.boot:spring-boot-starter-actuator")

		implementation("org.springframework.boot:spring-boot-starter-data-jdbc")
		implementation("com.alibaba:druid-spring-boot-starter:1.2.14")
		implementation("com.baomidou:mybatis-plus-boot-starter:3.5.2")
		implementation("com.baomidou:mybatis-plus-generator:3.5.2")
		implementation("com.github.pagehelper:pagehelper-spring-boot-starter:1.4.5")

//		implementation("org.springframework.boot:spring-boot-starter-cache")
//		implementation("org.springframework.boot:spring-boot-starter-data-redis")

		implementation("io.springfox:springfox-boot-starter:3.0.0")
		implementation("com.github.xiaoymin:swagger-bootstrap-ui:1.9.6")

//		implementation("org.jetbrains.kotlin:kotlin-reflect")
//		implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
//		implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
		implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-xml")

		implementation("org.joda:joda-money:1.0.2")
		implementation("org.projectlombok:lombok")

		developmentOnly("org.springframework.boot:spring-boot-devtools")
		annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
		annotationProcessor("org.projectlombok:lombok")

		runtimeOnly("com.h2database:h2")

		testImplementation("org.springframework.boot:spring-boot-starter-test")
	}

	java.sourceCompatibility = JavaVersion.VERSION_11

	configurations {
		compileOnly {
			extendsFrom(configurations.annotationProcessor.get())
		}
	}

	tasks.withType<KotlinCompile> {
		kotlinOptions {
			freeCompilerArgs = listOf("-Xjsr305=strict")
			jvmTarget = "11"
		}
	}

	tasks.withType<Test> {
		useJUnitPlatform()
	}
}