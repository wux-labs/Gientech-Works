package com.wux.labs.spring.springbucks.controller;


import com.wux.labs.spring.springbucks.entity.OrderCoffe;
import com.wux.labs.spring.springbucks.service.OrderCoffeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api("订单咖啡接口")
@RestController
@RequestMapping("/orderCoffe")
public class OrderCoffeController {
    @Resource
    private OrderCoffeeService service;

    @ApiOperation("查询所有")
    @GetMapping("/list")
    public List<OrderCoffe> list() {
        return service.list();
    }
}