package com.wux.labs.spring.springbucks.controller;


import com.wux.labs.spring.springbucks.SpringBucksApplication;
import com.wux.labs.spring.springbucks.bean.NewInstanceBean;
import com.wux.labs.spring.springbucks.bean.NormalComponentBean;
import com.wux.labs.spring.springbucks.service.CoffeeService;
import com.wux.labs.spring.springbucks.service.OrderCoffeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api("Bean接口")
@RestController
@RequestMapping("/beans")
public class BeansController {
    /**
     * 获取Bean的方式1，通过@Resource注解获取
     */
    @Resource
    private OrderCoffeeService bean1;

    @Resource
    private CoffeeService bean2;

    /**
     * 获取Bean的方式2，通过@Autowired注解获取
     */
    @Autowired
    private NormalComponentBean bean3;

    @ApiOperation(value = "通过XML配置定义的Bean", notes = "这是Bean装配的方式1，通过XML定义一个Bean！")
    @GetMapping("/sayHello1")
    public String sayHello1() {
        return bean1.sayHello();
    }

    @ApiOperation(value = "通过Service注解定义的Bean", notes = "这是Bean装配的方式2，通过有明确角色的Service注解定义一个组件！")
    @GetMapping("/sayHello2")
    public String sayHello2() {
        return bean2.sayHello();
    }

    @ApiOperation(value = "通过Component注解定义的Bean", notes = "这是Bean装配的方式3，通过无明确角色的Component注解定义一个组件！")
    @GetMapping("/sayHello3")
    public String sayHello3() {
        return bean3.sayHello();
    }

    @ApiOperation(value = "通过创建实例对象定义的Bean", notes = "这是Bean装配的方式4，通过创建一个我的实例对象，然后将我定义成Bean！")
    @GetMapping("/sayHello4")
    public String sayHello4() {
        // 获取Bean的方式3，通过ApplicationContext.getBean()获取
        NewInstanceBean bean4 = SpringBucksApplication.applicationContext.getBean(NewInstanceBean.class);
        return bean4.sayHello();
    }
}