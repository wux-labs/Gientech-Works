package com.wux.labs.spring.springbucks.controller;


import com.wux.labs.spring.springbucks.entity.Order;
import com.wux.labs.spring.springbucks.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Api("订单接口")
@RestController
@RequestMapping("/order")
public class OrderController {
    @Resource
    private OrderService service;

    @ApiOperation("查询所有")
    @GetMapping("/list")
    public List<Order> list() {
        return service.list();
    }
}