package com.wux.labs.spring.springbucks.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wux.labs.spring.springbucks.entity.OrderCoffe;
import com.wux.labs.spring.springbucks.mapper.OrderCoffeMapper;
import com.wux.labs.spring.springbucks.service.OrderCoffeeService;

public class OrderCoffeeServiceImpl extends ServiceImpl<OrderCoffeMapper, OrderCoffe> implements OrderCoffeeService {
    @Override
    public String sayHello() {
        return "Hello! 我是一个普通的类，你可以通过XML将我定义成一个Bean，然后通过@ImportResource对我进行装配！";
    }
}