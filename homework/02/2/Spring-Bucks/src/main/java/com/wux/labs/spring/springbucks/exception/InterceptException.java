package com.wux.labs.spring.springbucks.exception;

/**
 * 自定义拦截异常类
 */
public class InterceptException extends Exception {
    public InterceptException(String message) {
        super(message);
    }
}
