package com.wux.labs.spring.springbucks.controller;


import com.wux.labs.spring.springbucks.entity.Coffee;
import com.wux.labs.spring.springbucks.exception.RollbackException;
import com.wux.labs.spring.springbucks.service.CoffeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Api("咖啡接口")
@RestController
@RequestMapping("/coffee")
public class CoffeeController {
    @Autowired
    private CoffeeService service;

    @ApiOperation("查询所有")
    @GetMapping("/list")
    public List<Coffee> list() {
        return service.list();
    }

    @ApiOperation("分页查询")
    @PostMapping("/listByPage")
    public List<Coffee> listByPage(Long pageNum, Long pageSize) {
        return service.listByPage(pageNum, pageSize);
    }

    @ApiOperation("批量查询")
    @PostMapping("/listByIds")
    public List<Coffee> listByIds(Long... ids) {
        return service.listByIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

    @ApiOperation("完整插入")
    @PutMapping("/create")
    public boolean create(Coffee coffee) {
        try {
            return service.create(coffee);
        } catch (RollbackException e) {
            return false;
        }
    }

    @ApiOperation(value = "简单插入", notes = "当输入 name = 'rollback' 时，事务会回滚。")
    @PutMapping("/simpleCreate")
    public String simpleCreate(String name, Double price) {
        try {
            service.create(Coffee.builder()
                    .name(name)
                    .price(Money.of(CurrencyUnit.of("CNY"), price))
                    .createTime(new Date())
                    .updateTime(new Date())
                    .build());
            return "成功插入记录！";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @ApiOperation(value = "简单更新", notes = "当输入 name = 'rollback' 时，事务会回滚。")
    @PostMapping("/simpleUpdate")
    public String simpleUpdate(Long id, String name, Double price) {
        try {
            int update = service.update(Coffee.builder()
                    .id(id)
                    .name(name)
                    .price(Money.of(CurrencyUnit.of("CNY"), price))
                    .updateTime(new Date())
                    .build());
            return "成功更新了" + update + "条记录！";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @ApiOperation("查询记录")
    @GetMapping("/selectById")
    public Coffee selectById(Long id) {
        return service.getById(id);
    }

    @ApiOperation(value = "删除记录", notes = "当删除的 id 在 [1,2,3,4,5] 中时，事务会回滚。")
    @DeleteMapping("/deleteById")
    public String deleteById(Long id) {
        try {
            int delete = service.delete(id);
            return "成功删除了" + delete + "条记录！";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @ApiOperation("批量查询，返回JSON")
    @PostMapping(value = "/listByIdsJson", produces = {"application/json"})
    public List<Coffee> listByIdsJson(Long... ids) {
        return service.listByIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

    @ApiOperation("批量查询，返回XML")
    @PostMapping(value = "/listByIdsXml", produces = {"application/xml"})
    public List<Coffee> listByIdsXml(Long... ids) {
        return service.listByIds(Arrays.stream(ids).collect(Collectors.toList()));
    }

}