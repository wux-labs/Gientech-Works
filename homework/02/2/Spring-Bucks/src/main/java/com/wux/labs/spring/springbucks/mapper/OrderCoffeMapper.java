package com.wux.labs.spring.springbucks.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wux.labs.spring.springbucks.entity.OrderCoffe;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderCoffeMapper extends BaseMapper<OrderCoffe> {
}
