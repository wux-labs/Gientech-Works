package com.wux.labs.spring.springbucks.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wux.labs.spring.springbucks.entity.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
