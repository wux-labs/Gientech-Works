package com.wux.labs.spring.springbucks.bean;

public class NewInstanceBean {
    public String sayHello() {
        return "Hello! 我是一个普通的类，你可以创建一个我的实例对象，然后将我定义成Bean！";
    }
}
