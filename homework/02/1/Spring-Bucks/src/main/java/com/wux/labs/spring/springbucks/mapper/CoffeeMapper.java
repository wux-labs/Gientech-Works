package com.wux.labs.spring.springbucks.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wux.labs.spring.springbucks.entity.Coffee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CoffeeMapper extends BaseMapper<Coffee> {

    /**
     * 分页查询
     */
    @Select("select * from t_coffee order by id")
    List<Coffee> listByPage(@Param("pageNum") Long pageNum, @Param("pageSize") Long pageSize);

    /**
     * 无侵入式校验拦截
     */
    void interceptFilter(String sql);
}
