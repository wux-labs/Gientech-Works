package com.wux.labs.spring.springbucks.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wux.labs.spring.springbucks.entity.OrderCoffe;

public interface OrderCoffeeService extends IService<OrderCoffe> {
    String sayHello();
}