package com.wux.labs.spring.springbucks.configuration;

import com.wux.labs.spring.springbucks.bean.NewInstanceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    /**
     * Bean装配的方式4，通过创建一个类的实例对象来装配一个Bean
     */
    @Bean
    public NewInstanceBean createNewInstanceBean() {
        return new NewInstanceBean();
    }
}
