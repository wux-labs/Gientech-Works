package com.wux.labs.spring.springbucks.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wux.labs.spring.springbucks.entity.Order;
import com.wux.labs.spring.springbucks.exception.RollbackException;
import com.wux.labs.spring.springbucks.mapper.OrderMapper;
import com.wux.labs.spring.springbucks.service.OrderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("orderService")
@Transactional(rollbackFor = {RollbackException.class})
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {
}