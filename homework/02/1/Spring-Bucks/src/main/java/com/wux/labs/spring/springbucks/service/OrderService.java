package com.wux.labs.spring.springbucks.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wux.labs.spring.springbucks.entity.Order;

public interface OrderService extends IService<Order> {
}