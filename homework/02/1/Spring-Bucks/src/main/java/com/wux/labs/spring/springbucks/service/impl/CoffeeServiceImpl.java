package com.wux.labs.spring.springbucks.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wux.labs.spring.springbucks.entity.Coffee;
import com.wux.labs.spring.springbucks.exception.RollbackException;
import com.wux.labs.spring.springbucks.mapper.CoffeeMapper;
import com.wux.labs.spring.springbucks.service.CoffeeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean装配的方式2，通过有明确角色的Service注解定义一个组件
 */
@Service("coffeeService")
@Transactional(rollbackFor = {RollbackException.class})
public class CoffeeServiceImpl extends ServiceImpl<CoffeeMapper, Coffee> implements CoffeeService {
    /**
     * 分页查询
     */
    @Override
    public List<Coffee> listByPage(Long pageNum, Long pageSize) {
        return baseMapper.listByPage(pageNum, pageSize);
    }

    /**
     * 创建记录
     */
    @Override
    public boolean create(Coffee coffee) throws RollbackException {
        boolean result = save(coffee);

        if (coffee.getName() == "rollback") {
            throw new RollbackException("咖啡的名称是：'rollback'，所以回滚了！");
        }

        return result;
    }

    /**
     * 修改记录
     */
    @Override
    public int update(Coffee coffee) throws RollbackException {
        int result = baseMapper.updateById(coffee);

        if (coffee.getName() == "rollback") {
            throw new RollbackException("咖啡的名称是：'rollback'，所以回滚了！");
        }

        return result;
    }

    /**
     * 删除记录
     */
    @Override
    public int delete(Long id) throws RollbackException {
        int result = baseMapper.deleteById(id);

        List list = new ArrayList<Long>();
        list.add(1L);
        list.add(2L);
        list.add(3L);
        list.add(4L);
        list.add(5L);

        if (list.contains(id)) {
            throw new RollbackException("删除的记录ID在[1,2,3,4,5]中，所以回滚了！");
        }

        return result;
    }

    /**
     * 无侵入式校验拦截
     */
    @Override
    public void interceptFilter(String sql) {
        baseMapper.interceptFilter(sql);
    }

    @Override
    public String sayHello() {
        return "Hello! 我是一个有明确角色的org.springframework.stereotype.Service注解定义的一个Service组件！";
    }
}