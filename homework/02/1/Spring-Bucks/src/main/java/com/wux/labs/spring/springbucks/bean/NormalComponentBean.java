package com.wux.labs.spring.springbucks.bean;

import org.springframework.stereotype.Component;

/**
 * Bean装配的方式3，通过无明确角色的Component注解定义一个组件
 */
@Component
public class NormalComponentBean {
    public String sayHello() {
        return "Hello! 我是一个通过没有明确角色的org.springframework.stereotype.Component注解装配的组件！";
    }
}
