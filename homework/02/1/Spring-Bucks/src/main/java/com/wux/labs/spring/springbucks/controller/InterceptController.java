package com.wux.labs.spring.springbucks.controller;

import com.wux.labs.spring.springbucks.service.CoffeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("拦截接口")
@RestController
@RequestMapping("/intercept")
public class InterceptController {
    @Autowired
    private CoffeeService service;

    @ApiOperation(value = "无侵入式校验拦截", notes = "需要手工输入完整是SQL语句进行拦截校验。")
    @GetMapping("/interceptFilter")
    public String interceptFilter(String sql) {
        try {
            service.interceptFilter(sql);
            return "执行成功";
        } catch (UncategorizedSQLException ex) {
            return ex.getRootCause().getMessage();
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }
}
