package com.wux.labs.spring.springbucks.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@TableName("T_ORDER_COFFEE")
@ApiModel(value = "订单表")
public class OrderCoffe extends Model<OrderCoffe> {

    private Long coffeeOrderId;
    private Long itemsId;

}