package com.wux.labs.spring.springbucks.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wux.labs.spring.springbucks.entity.Coffee;
import com.wux.labs.spring.springbucks.exception.RollbackException;

import java.util.List;

public interface CoffeeService extends IService<Coffee> {
    /**
     * 分页查询
     */
    List<Coffee> listByPage(Long pageNum, Long pageSize);

    /**
     * 创建记录
     */
    boolean create(Coffee coffee) throws RollbackException;

    /**
     * 修改记录
     */
    int update(Coffee coffee) throws RollbackException;

    /**
     * 删除记录
     */
    int delete(Long id) throws RollbackException;

    /**
     * 无侵入式校验拦截
     */
    void interceptFilter(String sql);

    String sayHello();
}