package com.wux.labs.spring.springbucks.exception;

/**
 * 自定义回滚异常类
 */
public class RollbackException extends Exception {
    public RollbackException(String message) {
        super(message);
    }
}
