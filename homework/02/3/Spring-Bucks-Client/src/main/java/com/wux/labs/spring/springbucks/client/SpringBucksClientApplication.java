package com.wux.labs.spring.springbucks.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.wux.labs.spring.springbucks.client"})
public class SpringBucksClientApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringBucksClientApplication.class, args);
    }
}
