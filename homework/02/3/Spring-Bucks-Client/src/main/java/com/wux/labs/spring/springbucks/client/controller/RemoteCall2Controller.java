package com.wux.labs.spring.springbucks.client.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.stream.Collectors;

@Api("远程调用接口")
@RestController
@RequestMapping("/remote2")
public class RemoteCall2Controller {
    @Resource
    private RestTemplate restTemplate;

    @ApiOperation(value = "方式2，通过RestTemplate调用Json", notes = "通过RestTemplate调用远程返回Json数据的接口！")
    @PostMapping("/restTemplateRemoteJson")
    public String restTemplateRemoteJson(Long... ids) {
        MultiValueMap request = new LinkedMultiValueMap();
        request.put("ids", Arrays.stream(ids).collect(Collectors.toList()));
        return restTemplate.postForObject("http://localhost:8080/coffee/listByIdsJson", request, String.class);
    }

    @ApiOperation(value = "方式2，通过RestTemplate调用XML", notes = "通过RestTemplate调用远程返回XML数据的接口！")
    @PostMapping("/restTemplateRemoteXml")
    public String restTemplateRemoteXml(Long... ids) {
        MultiValueMap request = new LinkedMultiValueMap();
        request.put("ids", Arrays.stream(ids).collect(Collectors.toList()));
        return restTemplate.postForObject("http://localhost:8080/coffee/listByIdsXml", request, String.class);
    }
}