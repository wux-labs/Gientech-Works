package com.wux.labs.spring.springbucks.client.feign;

import org.springframework.stereotype.Component;

@Component
public class FeignRemoteFallback implements FeignRemoteCall {
    @Override
    public String listByIdsJson(Long... ids) {
        return "Fallback";
    }

    @Override
    public String listByIdsXml(Long... ids) {
        return "Fallback";
    }
}
