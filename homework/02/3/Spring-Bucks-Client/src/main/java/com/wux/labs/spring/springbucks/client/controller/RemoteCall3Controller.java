package com.wux.labs.spring.springbucks.client.controller;


import com.wux.labs.spring.springbucks.client.feign.FeignRemoteCall;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.stream.Collectors;

@Api("远程调用接口")
@RestController
@RequestMapping("/remote3")
public class RemoteCall3Controller {
    @Resource
    private FeignRemoteCall feignRemoteCall;

    @ApiOperation(value = "方式3，通过FeignClient调用Json", notes = "通过FeignClient调用远程返回Json数据的接口！")
    @PostMapping("/feignClientRemoteJson")
    public String feignClientRemoteJson(Long... ids) {
        return feignRemoteCall.listByIdsJson(ids);
    }

    @ApiOperation(value = "方式3，通过FeignClient调用XML", notes = "通过FeignClient调用远程返回XML数据的接口！")
    @PostMapping("/feignClientRemoteXml")
    public String feignClientRemoteXml(Long... ids) {
        return feignRemoteCall.listByIdsXml(ids);
    }
}