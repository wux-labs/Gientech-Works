package com.wux.labs.spring.springbucks.client.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

@Api("远程调用接口")
@RestController
@RequestMapping("/remote1")
public class RemoteCall1Controller {

    @Resource
    private HttpClient httpClient;

    @ApiOperation(value = "方式1，通过HttpClient调用Json", notes = "通过HttpClient调用远程返回Json数据的接口！")
    @PostMapping("/httpClientRemoteJson")
    public String httpClientRemoteJson() throws IOException {
        HttpPost post = new HttpPost("http://localhost:8080/coffee/listByIdsJson?ids=1,2,3");
        return new String(httpClient.execute(post).getEntity().getContent().readAllBytes());
    }

    @ApiOperation(value = "方式1，通过HttpClient调用XML", notes = "通过HttpClient调用远程返回XML数据的接口！")
    @PostMapping("/httpClientRemoteXml")
    public String httpClientRemoteXml() throws IOException {
        HttpPost post = new HttpPost("http://localhost:8080/coffee/listByIdsXml?ids=1,2,3");
        return new String(httpClient.execute(post).getEntity().getContent().readAllBytes());
    }
}