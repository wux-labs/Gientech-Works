package com.wux.labs.spring.springbucks.client.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "http://localhost:8080", name = "feignRemoteCall", fallback = FeignRemoteFallback.class)
public interface FeignRemoteCall {

    @PostMapping("/coffee/listByIdsJson")
    String listByIdsJson(@RequestParam("ids") Long... ids);

    @PostMapping("/coffee/listByIdsXml")
    String listByIdsXml(@RequestParam("ids") Long... ids);
}
