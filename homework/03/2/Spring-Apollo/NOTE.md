# 笔记

## Actuator监控

解禁Endpoint：
* 解禁所有：`management.endpoints.web.exposure.include=*`
* 解禁指定：`management.endpoints.web.exposure.include=health,beans,info`

访问监控：[http://localhost:8080/actuator/health](http://localhost:8080/actuator/health)

## H2数据库

访问内置控制台：[http://localhost:8080/h2-console](http://localhost:8080/h2-console)

## Druid数据源

启用监控：`spring.datasource.druid.stat-view-servlet.enabled = true`

访问监控界面：[http://localhost:8080/druid](http://localhost:8080/druid)

## Swagger3

默认接口界面：[http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/)

三方文档界面：[http://localhost:8080/doc.html](http://localhost:8080/doc.html)

## Mybatis

更新策略：

FieldStrategy 有三种策略：
* IGNORED：0 忽略
* NOT_NULL：1 非 NULL，默认策略
* NOT_EMPTY：2 非空