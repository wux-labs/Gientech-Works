package com.wux.labs.spring.apollo;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableApolloConfig
public class SpringApolloApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringApolloApplication.class, args);
    }
}
