package com.wux.labs.spring.apollo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("Apollo配置中心接口")
@RestController
@RequestMapping("/apollo")
public class ApolloController {

    @Value(value = "${apollo_config_key}")
    private String config;

    @ApiOperation("从Apollo获取配置信息")
    @PostMapping("/getConfig")
    public String getConfig() {
        return "获取到Apollo的配置：" + config;
    }
}
