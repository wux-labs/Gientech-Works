package com.wux.labs.spring.zookeeper.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api("生产者接口")
@RestController
@RequestMapping("/producer")
public class ProducerController {

    @Autowired
    private DiscoveryClient discoveryClient;

    @ApiOperation("服务列表")
    @PostMapping("/list")
    public List<String> list() {
        return discoveryClient.getServices();
    }

    @ApiOperation("生产者测试")
    @PostMapping("/test")
    public String test() {
        return "生产者测试";
    }
}
