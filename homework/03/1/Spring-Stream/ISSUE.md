# 问题

## Spring Boot 2.6.x+ 整合 Swagger3

### Failed to start bean 'documentationPluginsBootstrapper'

Springfox设置Spring MVC的路径匹配策略是ant-path-matcher，而Spring Boot 2.6.x版本的默认匹配策略是path-pattern-matcher。

这会造成报错`org.springframework.context.ApplicationContextException: Failed to start bean 'documentationPluginsBootstrapper'; nested exception is java.lang.NullPointerException`

解决办法：

1、设置：`spring.mvc.pathmatch.matching-strategy:ant_path_matcher`

2、配置Bean
```kotlin
    @Bean
    fun springfoxHandlerProviderBeanPostProcessor(): BeanPostProcessor {
        return object : BeanPostProcessor {
            override fun postProcessAfterInitialization(bean: Any, beanName: String): Any {
                if (bean is WebMvcRequestHandlerProvider || bean is WebFluxRequestHandlerProvider) {
                    customizeSpringfoxHandlerMappings(getHandlerMappings(bean))
                }
                return bean
            }

            private fun <T : RequestMappingInfoHandlerMapping> customizeSpringfoxHandlerMappings(mappings: MutableList<T>) {
                val copy = mappings.stream()
                    .filter { mapping: T -> mapping.patternParser == null }
                    .collect(Collectors.toList())
                mappings.clear()
                mappings.addAll(copy)
            }

            private fun getHandlerMappings(bean: Any): MutableList<RequestMappingInfoHandlerMapping> {
                return try {
                    val field: Field = ReflectionUtils.findField(bean.javaClass, "handlerMappings")!!
                    field.isAccessible = true
                    field.get(bean) as MutableList<RequestMappingInfoHandlerMapping>
                } catch (e: IllegalArgumentException) {
                    throw IllegalStateException(e)
                } catch (e: IllegalAccessException) {
                    throw IllegalStateException(e)
                }
            }
        }
    }
```

## Redis缓存

### Could not autowire. No beans of 'RedisConnectionFactory' type found.

## Lombok

### error: cannot find symbol

在build.gradle中添加`annotationProcessor("org.projectlombok:lombok")`
