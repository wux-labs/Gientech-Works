package com.wux.labs.spring.stream.processor;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Component;

@Component
public interface ConsumersProcessor {
    @Input("inputKafka")
    SubscribableChannel inputKafka();

    @Input("inputRabbit")
    SubscribableChannel inputRabbit();
}