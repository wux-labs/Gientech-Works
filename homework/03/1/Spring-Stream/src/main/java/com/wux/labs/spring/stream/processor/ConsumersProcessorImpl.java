package com.wux.labs.spring.stream.processor;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;

@EnableBinding(ConsumersProcessor.class)
public class ConsumersProcessorImpl {
    @StreamListener("inputKafka")
    public void inputKafka(Object message) {
        System.out.println("接收到Kafka消息：" + new String((byte[]) message));
    }

    @StreamListener("inputRabbit")
    public void inputRabbit(Object message) {
        System.out.println("接收到Rabbit消息：" + new String((byte[]) message));
    }
}
