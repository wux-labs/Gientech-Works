package com.wux.labs.spring.stream.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api("生产者接口")
@RestController
@RequestMapping("/send")
public class SendController {
    @Autowired
    private StreamBridge bridge;

    @ApiOperation("向Kafka发送数据")
    @PostMapping("/kafka")
    public String sendToKafka(String message) {
        boolean status = bridge.send("outputKafka", message);
        return "发送消息：" + message + "=====>" + status;
    }

    @ApiOperation("向Rabbit发送数据")
    @PostMapping("/rabbit")
    public String sendToRabbit(String message) {
        boolean status = bridge.send("outputRabbit", MessageBuilder.withPayload(message)
                .setHeader("routingKey", "first")
                .build());
        return "发送消息：" + message + "=====>" + status;
    }
}
