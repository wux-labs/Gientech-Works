# 中电金信分布式培训

## 目录结构

```
|- homework          # 作业
|  |- 01             # 第一次作业
|  |  |- 中电金信分布式03期初级班第一次作业.md
|  |  |- 1           # 题目1 使用 Druid + Mybatis + Redis 改造 springbucks 项目
|  |  |- 2           # 题目2 使用 Druid 实现无侵入式 SQL 校验功能
|  |  |- 3           # 题目3 搭建 Redis Cluster
|  |  |- 4           # 题目4 命令行中练习 Redis 相关的各种数据结构的命令操作
|  |  `- 中电金信分布式03期初级班第一次作业-演示.md
|  |- 02             # 第二次作业
|  |  |- 中电金信分布式初级班03期第二次作业.md
|  |  |- 1           # 题目1 写代码实现 Spring Bean的装配，方式越多越好（XML、Annotation 都可以）
|  |  |- 2           # 题目2 通过springmvc实现两个restful查询数据接口，一个返回json，一个返回xml
|  |  |- 3           # 题目3 新建一个Springboot项目，通过远程调用方式访问作业2提供的web接口
|  |  `- 中电金信分布式初级班03期第二次作业-演示.md
|  |- 03             # 第三次作业
|  |  |- 中电金信分布式初级班03期第3次作业.md
|  |  |- 1           # 题目1 通过 spring stream 访问 rabbit mq 与kafka
|  |  |- 2           # 题目2 整合apollo 作为服务配置中心
|  |  |- 3           # 题目3 用zookeeper作为服务注册中心，并通过服务发现正确发起远程调用
|  |  `- 中电金信分布式初级班03期第3次作业-演示.md

```

